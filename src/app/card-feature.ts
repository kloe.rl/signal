export interface CardFeature {
    img: string;
    heading: string;
    description: string;
}
