import { Component } from '@angular/core';

@Component({
  selector: 'app-button-clear',
  standalone: true,
  imports: [],
  template: `
    <section>
      <a class="button button2" href="/fr/donate/"> Faire un don à Signal </a>
    </section>
  `,
  // templateUrl: './button-clear.component.html',
  styleUrl: './button-clear.component.scss',
})
export class ButtonClearComponent {}
