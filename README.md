# Contexte du projet

L'équipe technique du site de l'application de communication Signal souhaite passer sur la technologie Angular.
Afin d'en tester la faisabilité, il t'est demandé de réintégrer la page d'accueil (et uniquement celle-ci) sur Angular.
La page est la suivante : https://signal.org/fr/.
Tu trouveras les images dans une archive en ressource. Tu ne dois pas faire l'animation en haut de page, tu peux la remplacer par l'image animation.png de l'archive.
La police de caractère utilisée est Inter.
Seule la version navigateur (desktop) est demandée.

# Signal

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
